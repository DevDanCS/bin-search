#pragma once

int binarySearch(auto Data, auto key)
{
	int first = 0;
	int last = Data.size() - 1;
	int mid = 0;  
	
	while(first <= last)//more elements
	{ 
            mid = (first+last)/2;//calc middle
		
		if(Data[mid] == key)//found it
		  return mid;
		
		if (key < Data[mid])
		   last = mid - 1;//search left side of list
		else
		   first = mid + 1;//search right side of list 
		     
	}//end while
	
	   return -1;//element not found
}

